FROM ruby:2.1-alpine

RUN apk update && apk add --no-cache build-base mysql-dev
COPY ./ /geocaching
WORKDIR /geocaching
RUN bundle install
